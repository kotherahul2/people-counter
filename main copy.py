import cv2
import pandas as pd
from ultralytics import YOLO
from tracker import *
import cvzone
import numpy as np

model = YOLO('yolov8m.pt')

def RGB(event, x, y, flags, param):
    if event == cv2.EVENT_MOUSEMOVE:
        point = [x, y]
        print(point)

cv2.namedWindow('RGB')
cv2.setMouseCallback('RGB', RGB)
cap = cv2.VideoCapture(r'test_1.mp4')

my_file = open("coco.txt", "r")
data = my_file.read()
class_list = data.split("\n")

count = 0
person_down = {}
tracker = Tracker()
counter1 = []

person_up = {}
counter2 = []
cy1 = 194
cy2 = 220
offset = 6

# Define vertices for polylines
vertices_down = np.array([[3, cy1], [1018, cy1]], np.int32)
vertices_up = np.array([[5, cy2], [1019, cy2]], np.int32)

while True:
    ret, frame = cap.read()
    if not ret:
        break

    count += 1
    if count % 3 != 0:
        continue

    frame = cv2.resize(frame, (1020, 500))

    results = model.predict(frame)
    a = results[0].boxes.data
    px = pd.DataFrame(a).astype("float")

    list = []
    for index, row in px.iterrows():
        x1, y1, x2, y2, _, d = map(int, row[:6])
        c = class_list[d]
        if 'person' in c:
            list.append([x1, y1, x2, y2])

    bbox_id = tracker.update(list)
    for bbox in bbox_id:
        x3, y3, x4, y4, id = bbox
        cx, cy = (int(x3 + x4) // 2, int(y3 + y4) // 2)
        cv2.circle(frame, (cx, cy), 4, (255, 0, 255), -1)

        # Down count
        if cv2.pointPolygonTest(vertices_down, (cx, cy), False) >= 0:
            cv2.rectangle(frame, (x3, y3), (x4, y4), (0, 0, 255), 2)
            cvzone.putTextRect(frame, f'{id}', (x3, y3), 1, 2)
            person_down[id] = (cx, cy)

        if id in person_down and cv2.pointPolygonTest(vertices_up, (cx, cy), False) >= 0:
            cv2.rectangle(frame, (x3, y3), (x4, y4), (0, 255, 255), 2)
            cvzone.putTextRect(frame, f'{id}', (x3, y3), 1, 2)
            if id not in counter1:
                counter1.append(id)

        # Up count
        if cv2.pointPolygonTest(vertices_up, (cx, cy), False) >= 0:
            cv2.rectangle(frame, (x3, y3), (x4, y4), (0, 0, 255), 2)
            cvzone.putTextRect(frame, f'{id}', (x3, y3), 1, 2)
            person_up[id] = (cx, cy)

        if id in person_up and cv2.pointPolygonTest(vertices_down, (cx, cy), False) >= 0:
            cv2.rectangle(frame, (x3, y3), (x4, y4), (0, 255, 255), 2)
            cvzone.putTextRect(frame, f'{id}', (x3, y3), 1, 2)
            if id not in counter2:
                counter2.append(id)

    cv2.polylines(frame, [vertices_down], isClosed=False, color=(0, 255, 0), thickness=2)
    cv2.polylines(frame, [vertices_up], isClosed=False, color=(0, 255, 255), thickness=2)

    down = len(counter1)
    up = len(counter2)

    cvzone.putTextRect(frame, f'Down: {down}', (50, 60), 2, 2)
    cvzone.putTextRect(frame, f'Up: {up}', (50, 160), 2, 2)

    cv2.imshow("RGB", frame)
    if cv2.waitKey(1) & 0xFF == 27:
        break

cap.release()
cv2.destroyAllWindows()
